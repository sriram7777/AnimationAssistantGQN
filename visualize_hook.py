import cv2
import glob
import os

from config import *
from google_drive.drive_utils import *


def store_and_upload_frames(data, target, recreated, step):
    target = cv2.cvtColor(target, cv2.COLOR_RGB2BGR)
    cv2.imwrite('visual/target_frame.jpg', target)
    recreated = cv2.cvtColor(recreated, cv2.COLOR_RGB2BGR)
    cv2.imwrite('visual/recreated_frame.jpg', target)
    with open('camera_pose.txt', 'w') as f:
        f.write('pose variables:\t x    y   z   yaw   pitch')
        index = 0
        for frame, pose in zip(data.context.frames[0], data.context.cameras[0]):
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
            cv2.imwrite('visual/frame_ ' + str(index) + '.jpg', frame)
            f.write("camera pose {}:\t {}".format(str(index), *pose))
            index += 1
        f.write("query pose:\t {}".format(*data.query_camera[0]))

    f.close()

    # zipping all the files of the visual folder
    create_archive("visuals_ " + str(step) + ".tar.gz", "/visual", verbose=True)

    # uploading the zipped file
    driver_handler.upload("visuals_ " + str(step) + ".tar.gz", DRIVE_VISUAL_PATH)

    # clearing the visual folder
    files = glob.glob("/visual")
    for file in files:
        os.remove(file)
