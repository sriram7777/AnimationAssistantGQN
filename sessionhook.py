import tensorflow as tf
from google_drive.drive_utils import *
from config import *


class CheckpointUploadHook(tf.train.CheckpointSaverHook):

    def __init__(self, checkpoint_dir, save_secs=None, save_steps=None, saver=None,
                 checkpoint_basename='model.ckpt', scaffold=None, listeners=None):
        super().__init__(checkpoint_dir, save_secs, save_steps, saver, checkpoint_basename, scaffold, listeners)
        self.dir = checkpoint_dir

    def _upload_chkpts(self):
        # Zipping the files of the checkpoint directory
        create_archive("checkpoints_latest.tar.gz", self.dir, verbose=True)

        # Uploading the zipped file
        driver_handler.upload("checkpoints_latest.tar.gz", DRIVE_CHECKPOINTS_PATH)

        os.remove("checkpoints_latest.tar.gz")

    def after_run(self, rc, rv):
        # get global step
        super().after_run(rc, rv)
        self.global_step = rc.session.run(self._global_step_tensor)
        if self._timer.should_trigger_for_step(self.global_step):
            self._upload_chkpts()

    def end(self, session):
        super().end(session)
        self.global_step = session.run(self._global_step_tensor)
        if self.global_step != self._timer.last_triggered_step():
            self._upload_chkpts()
