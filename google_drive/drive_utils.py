import os
import subprocess
from pathlib import Path
from google_drive.google_drive_handler import GoogleDriveHandler

driver_handler = GoogleDriveHandler()


def create_google_drive_structure(folders):
    for folder in folders:
        splits = folder.split('/')
        folder = splits[-1]
        root = '/'.join(splits[:-1])
        print("folder is {}".format(folder))
        print("root is {}".format(root))
        folder_id=driver_handler.create_folder(folder, root)
    return


def create_archive(zip_name, local_file_paths, temp_folder='/tmp', verbose=False):
    zip_name = '{0}/{1}'.format(temp_folder, zip_name) + '.tar.gz' * ('.tar.gz' not in zip_name)
    # Filter out non-existing files and directorys
    zipped_files = []
    for f in local_file_paths:
        if not Path(f).exists():
            print('file {0} does not exist, ignore it'.format(f))
        else:
            zipped_files.append(f)
    # Find common prefix to avoid a too many level folders
    common_prefix = ''
    for chars in zip(*zipped_files):
        if len(set(chars)) == 1:
            common_prefix += chars[0]
        else:
            break
    common_prefix = '/'.join(common_prefix.split('/')[:-1]) + '/'
    # Excuting tar.gz format compression
    L = len(common_prefix)
    zipped_files = ' '.join([f[L:] for f in zipped_files])
    cmd = 'tar -czvf {0} -C {1} {2}'.format(zip_name, common_prefix, zipped_files)
    if verbose:
        print('ignore the common prefix {0}'.format(common_prefix))
        print('running shell command:', '\n' + cmd)
    result = subprocess.check_output(cmd, shell=True).decode('utf-8')
    if verbose: print(result)
    # Return absolute path of the tar.gz file
    return zip_name


def extract_archive(zip_path, target_folder='./', verbose=False):
    cmd = 'tar -xf {0} -C {1}'.format(zip_path, target_folder)
    if verbose: print('running shell command:', '\n' + cmd)
    result = subprocess.check_output(cmd, shell=True).decode('utf-8')
    if verbose: print(result)
